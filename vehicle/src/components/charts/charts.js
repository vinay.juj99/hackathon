
import React, { useState } from 'react';
import './charts.css';
import { RadioButton } from 'primereact/radiobutton';
import ManufacturerChartDemo from './manufacturerCharts';
import FuelChartDemo from './fuelcharts';
import CurrencyChartDemo from './currencyCharts';
import CountryChartDemo from './countryCharts';

const PieChartDemo = () => {
    const [showManufactChart , setShowManufactChart] = useState(true);
    const [showFuelChart , setShowFuelChart] = useState();
    const [showCountryfactChart , setShowCountryChart] = useState();
    const [showCurrencyChart , setShowCurrencyChart] = useState();
    const [city, setCity] = useState(null);
 
    const onChangeRadioManufac = (e) => {
        setCity(e.value)
        setShowManufactChart(true);
        setShowCountryChart(false);
        setShowCurrencyChart(false);
        setShowFuelChart(false);
    }
    const onChangeRadioFuel = (e) => {
        setCity(e.value)
        setShowManufactChart(false);
        setShowCountryChart(false);
        setShowCurrencyChart(false);
        setShowFuelChart(true);
    }
    const onChangeRadioCountry = (e) => {
        setCity(e.value)
        setShowManufactChart(false);
        setShowCountryChart(true);
        setShowCurrencyChart(false);
        setShowFuelChart(false);
    }
    const onChangeRadioCurrency = (e) => {
        setCity(e.value)
        setShowManufactChart(false);
        setShowCountryChart(false);
        setShowCurrencyChart(true);
        setShowFuelChart(false);
    }

    return (
        <div className="card flex justify-content-center">
            <div className='checkbuttons'>
                <div className="radiobutton">
                    <RadioButton className='' inputId="city1" name="city" value="Chicago" onChange={onChangeRadioManufac} checked={city === 'Manufacturer'} />
                    <label htmlFor="city1">Manufacturer</label>
                </div>
                <div className="radiobutton">
                    <RadioButton inputId="city2" name="city" value="Los Angeles" onChange={onChangeRadioFuel} checked={city === 'Fuel'} />
                    <label htmlFor="city2">Fuel</label>
                </div>
                <div className="radiobutton">
                    <RadioButton inputId="city3" name="city" value="New York" onChange={onChangeRadioCurrency} checked={city === 'Currency'} />
                    <label htmlFor="city3">Currency</label>
                </div>
                <div className="radiobutton">
                    <RadioButton inputId="city4" name="city" value="San Francisco" onChange={onChangeRadioCountry} checked={city === 'Country'} />
                    <label htmlFor="city4">Country</label>
                </div>
            </div>
            {showManufactChart ? <ManufacturerChartDemo/> : ""}
            {showFuelChart ? <FuelChartDemo/> : ""}
            {showCurrencyChart ? <CurrencyChartDemo/> : ""}
            {showCountryfactChart ? <CountryChartDemo/> : ""}
        </div>
    )
}
export default PieChartDemo              