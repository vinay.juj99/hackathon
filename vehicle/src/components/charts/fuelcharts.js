
import React ,{useState,useEffect}from 'react';
import Chart from 'react-apexcharts'

const FuelChartDemo = () => {
    const [data,setData] = useState([]);
    const[value,setValue] = useState([])

    useEffect(()=>{
        const labels =[]
        const counts = {}
        const getVehiclesData = async()=>{
            const fetchData = await fetch(`https://63037d409eb72a839d824580.mockapi.io/Vehicle`);
            const resData = await fetchData.json()
            console.log(resData,'totalData');
            for (var i of resData ){
                labels.push(i.fuel)
               
            }
            setData(labels)
            labels.forEach(function (x) { counts[x] = (counts[x] || 0) + 1; });

            
            setValue(Object.values(counts))
            console.log(value,"graphhhhh")
        }
        getVehiclesData();


    },[])


    return(
        <React.Fragment>
            <div>
                <Chart 
                type='pie'
                width={949}
                height={350}
                series={value}
                options={{
                    noData:{text:"Empty Data"},
                    labels:[...new Set(data)]

                }}
                >
                </Chart>
            </div>
        </React.Fragment>
    )
}
export default FuelChartDemo;