import axios from 'axios';

export const getAllData=async()=>{
    return await axios
    .get("https://63037d409eb72a839d824580.mockapi.io/Vehicle", {
      mode: "no-cors",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
      },
    })
    .then((res) => {
      let content=res.data;
      return content;
     
    })
    .catch((e) => console.log("error", e));
    }
  