import React, { useState, useRef, useEffect } from "react";
import { Sidebar } from "primereact/sidebar";
import { Button } from "primereact/button";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import axios from "axios";
import "./landingPage.css";
import RegisterData from "../vehicleRegister/register";
import PieChartDemo from "../charts/charts";
import { getAllData } from "../serivces/api";

function LandingPage() {
  const dt = useRef(null);
  const [tableData, setTableData] = useState();
  const [selectedVehicle, setSelectedVehicle] = useState(null);
  const [render, setRender] = useState(false);
  const [showCharts, setShowCharts] = useState(true);

  useEffect(() => {
    const getAllAPI = () => {
      axios
        .get("https://63037d409eb72a839d824580.mockapi.io/Vehicle", {
          mode: "no-cors",
          headers: {
            "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
          },
        })
        .then((res) => {
          console.log(res, "res");
          console.log(res.data);
          setTableData(res.data);
        })
        .catch((e) => console.log("error", e));
    };
    getAllAPI();
  }, []);

  if (render === true) {
    console.log("gggggg");
  } else {
    console.log("");
  }
  const getAllDataAfterDelete = async() => {
    let data = await getAllData()
    console.log(data, "Llll")
    setTableData(data);
  }
  const deleteApiCall = (id) => {
    console.log("hello dlete", id);
    axios
      .delete(`https://63037d409eb72a839d824580.mockapi.io/Vehicle/${id}`, {
        mode: "no-cors",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
        },
      })
      .then((res) => {
        console.log(res.data);
        console.log("deleted");
        getAllDataAfterDelete()
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const callDeleteapi = (id) => {
    console.log("hello" , id);
    deleteApiCall(id)
  };
  const actionBodyTemplate = (rowData) => {
    console.log(rowData.id, "rowdata");
    return (
      <React.Fragment>
        <Button
          icon="pi pi-trash"
          className=""
          onClick={()=>callDeleteapi(rowData.id)}
        />
      </React.Fragment>
    );
  };
  return (
    <div className="main-container">
      <RegisterData
        render={render}
        setRender={setRender}
        setShowCharts={setShowCharts}
        setTotalData= {setTableData}
      />
      {showCharts ? (
        <div className="table-container">
          <DataTable
            ref={dt}
            value={tableData}
            scrollable
            scrollHeight="100vh"
            selectionMode="checkbox"
            selection={selectedVehicle}
            onSelectionChange={(e) => setSelectedVehicle(e.value)}
            dataKey="id"
          >
            <Column
              selectionMode="multiple"
              headerStyle={{ width: "3em" }}
            ></Column>
            <Column
              field="name"
              header="NAME"
              style={{ minWidth: "12rem" }}
              className="column"
            ></Column>
            <Column
              field="manufacturer"
              header="MANUFACTURER"
              style={{ minWidth: "12rem" }}
            ></Column>
            <Column
              field="model"
              header="MODEL"
              style={{ minWidth: "10rem" }}
            ></Column>
            <Column
              field="price"
              header="FUEL"
              style={{ minWidth: "8rem" }}
            ></Column>
            <Column
              field="color"
              header="COLOR"
              style={{ minWidth: "8rem" }}
            ></Column>
            <Column
              field="price"
              header="PRICE"
              style={{ minWidth: "8rem" }}
            ></Column>
            <Column
              field="currency"
              header="CURRENCY"
              style={{ minWidth: "8rem" }}
            ></Column>
            <Column
              field="city"
              header="CITY"
              style={{ minWidth: "8rem" }}
            ></Column>
            <Column
              field="country"
              header="COUNTRY"
              style={{ minWidth: "10rem" }}
            ></Column>
            <Column
              body={actionBodyTemplate}
              exportable={false}
              style={{ minWidth: "8rem" }}
              className="delete"
            ></Column>
           
          </DataTable>
        </div>
      ) : (
        <PieChartDemo />
      )}
    </div>
  );
}

export default LandingPage;
