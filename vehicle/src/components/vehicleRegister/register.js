
import React, { useState } from 'react';
import { Sidebar } from 'primereact/sidebar';
import { getAllData } from '../serivces/api';
import { InputText } from 'primereact/inputtext';
import '../landingPage/landingPage.css';
import './register.css';
const RegisterData = (props) => {
    const [name, setName] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [model, setModel] = useState('');
    const [price, setPrice] = useState();
    const [color, setColor] = useState('');
    const [fuel, setFuel] = useState('');
    const [currency, setCurrency] = useState('');
    const [city, setCity] = useState('');
    const [country, setCountry] = useState('');
    const [visibleRight, setVisibleRight] = useState(false);

    const onChangeName = (e) => {
        setName(e.target.value)
    }

    const onChangeManufacturer = (e) => {
        setManufacturer(e.target.value)
    }

    const onChangePrice = (e) => {
        setPrice(e.target.value)
    }

    const onChangeModel = (e) => {
        setModel(e.target.value)
    }

    const onChangeColor = (e) => {
        setColor(e.target.value)
    }

    const onChangeFuel = (e) => {
        setFuel(e.target.value)
    }

    const onChangeCurrency = (e) => {
        setCurrency(e.target.value)
    }

    const onChangeCity = (e) => {
        setCity(e.target.value)
    }

    const onChangeCountry = (e) => {
        setCountry(e.target.value)
    }
    const getAllDataAfterAdd = async() => {
        let data = await getAllData()
        console.log(data, "added")
        // props.setTableData(data);
      }

    const AddSongSubmit = (e) => {
      e.preventDefault();
      const formData = {
        name: name,
        model: model,
        manufacturer: manufacturer,
        price: parseInt(price),
        city: city,
        country: country,
        fuel: fuel,
        color: color,
        currency: currency,
      };
      const option = {
        method: "POST",
        body: JSON.stringify(formData),
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      };
  
      const data = JSON.stringify(formData);
      console.log(data, "postdatra");
    fetch("https://63037d409eb72a839d824580.mockapi.io/Vehicle", option)
    .then((res)=>{
        console.log(res)
        getAllDataAfterAdd()
        setVisibleRight(false)
    })
    .catch((err)=>{
        console.log(err)
    })
    };
    const onClickShowCharts = () => {
        props.setShowCharts(false)
    }

    const onClickShowTable = () => {
        props.setShowCharts(true)
    }
    return (
        <div>
            <div className='header-part'>
                <h1 className='vehicle-heading'>Vehicle List</h1>
                <button className='add-vehicle' onClick={() => setVisibleRight(true)}> + Add Vehicle</button>
                <div>
                    <div className='register-buttons'>
                        <button className='button-icon'onClick={onClickShowTable}><i className="pi pi-align-center"></i></button>
                        <button className='button-icon' onClick={onClickShowCharts}><i className="pi pi-chart-bar"></i></button>
                    </div>
                </div>
            </div>
            <div className="card">
                <Sidebar visible={visibleRight} position="right" onHide={() => setVisibleRight(false)}>
                    <h3>Add a Vehicle</h3>
                    <p><span className='span'>*</span>All fields are mandatory</p>
                    <form onSubmit={AddSongSubmit}>
                        <div>
                        <span className="p-ml-2">Name</span><br></br>
                        <InputText value={name} onChange={onChangeName} />
                    </div>
                    <br></br>
                    <div>
                    <span className="p-ml-2">Manufacturer</span><br></br>
                    <InputText value={manufacturer} onChange={onChangeManufacturer} />
                    </div>
                    <br></br>
                    <div>
                    <span className="p-ml-2">Model</span><br></br>
                    <InputText value={model} onChange={onChangeModel} />
                    </div>
                    <br></br>
                    <div>
                    <span className="p-ml-2">Fuel</span><br></br>
                    <InputText value={fuel} onChange={onChangeFuel} />
                    </div>
                    <br></br>
                    <div>
                    <span className="p-ml-2">Color</span><br></br>
                    <InputText value={color} onChange={onChangeColor} />
                    </div>
                    <br></br>
                    <div>
                    <span className="p-ml-2">Price</span><br></br>
                    <InputText value={price} onChange={onChangePrice} />
                    <br></br>
                    </div>
                    <div>
                    <span className="p-ml-2">Currency</span><br></br>
                    <InputText value={currency} onChange={onChangeCurrency} />
                    <br></br>
                    </div>
                    <div>
                    <span className="p-ml-2">City</span><br></br>
                    <InputText value={city} onChange={onChangeCity} />
                    </div>
                    <br></br>
                    <div>
                    <span className="p-ml-2">Country</span><br></br>
                    <InputText value={country} onChange={onChangeCountry} />
                    </div>
                    <br></br>
                    <div className='form-buttons'>
                    <button className='add-vehicle' type="submit"> + Add Vehicle</button>
                    <button className='cancel-button' onClick={() => setVisibleRight(false)}> Cancel</button>
                    </div>
                    </form>
                </Sidebar>
            </div>
        </div>
    )
}

export default RegisterData;
                 